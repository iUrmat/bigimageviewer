﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using com.refractored.monodroidtoolkit;

namespace BigImageViewer.Droid
{
    [Activity(Label = "LargeImageActivity")]
    public class LargeImageActivity : Activity
    {
        protected async override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.LargeImageActivity);

            _originalDimensions = FindViewById<TextView>(Resource.Id.original_image_dimensions_textview);
            _resizedDimensions = FindViewById<TextView>(Resource.Id.resized_image_dimensions_textview);
            _imageView = FindViewById<ScaleImageView>(Resource.Id.resized_imageview);


            BitmapFactory.Options options = await GetBitmapOptionsOfImage();

            Bitmap bitmapToDisplay = await LoadScaledDownBitmapForDisplayAsync(Resources, options, options.OutWidth/10, options.OutHeight/10);
            _imageView.SetImageBitmap(bitmapToDisplay);

            _resizedDimensions.Text = string.Format("Reduced the image {0}x", options.InSampleSize);
        }


        ScaleImageView _imageView;
        TextView _originalDimensions;
        TextView _resizedDimensions;

        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            float height = options.OutHeight;
            float width = options.OutWidth;
            double inSampleSize = 1D;

            if (height > reqHeight || width > reqWidth)
            {
                int halfHeight = (int)(height / 2);
                int halfWidth = (int)(width / 2);

                // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
                while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }

            }

            return (int)inSampleSize;
        }

        public async Task<Bitmap> LoadScaledDownBitmapForDisplayAsync(Resources res, BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Calculate inSampleSize
            options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;

            return await BitmapFactory.DecodeResourceAsync(res, Resource.Drawable.MP600_300dpi, options);
        }

       


        async Task<BitmapFactory.Options> GetBitmapOptionsOfImage()
        {
            BitmapFactory.Options options = new BitmapFactory.Options
            {
                InJustDecodeBounds = true
            };

            // The result will be null because InJustDecodeBounds == true.
            Bitmap result = await BitmapFactory.DecodeResourceAsync(Resources, Resource.Drawable.MP600_300dpi, options);


            int imageHeight = options.OutHeight;
            int imageWidth = options.OutWidth;

            _originalDimensions.Text = string.Format("Original Size= {0}x{1}", imageWidth, imageHeight);

            return options;
        }
    }
}