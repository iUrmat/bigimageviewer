﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BigImageViewer.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ScrollView), typeof(ZoomRenderer))]
namespace BigImageViewer.iOS
{
    public class ZoomRenderer : ScrollViewRenderer
    {


        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            //zoom yakınlaşma seviyesi
            MaximumZoomScale = 10f;
            MinimumZoomScale = 1.0f;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();


            if (Subviews.Length > 0)
            {
                //zoomlama kontrolü son seviyeye kadar izin verip sonrasında
                //engelliyor
                ViewForZoomingInScrollView += GetViewForZomming;
            }
            else
            {

                ViewForZoomingInScrollView -= GetViewForZomming;
            }
        }

        private UIView GetViewForZomming(UIScrollView scrollView)
        {
            //zoom işlemi için gerekli olan metdoumuz
            return this.Subviews.FirstOrDefault();
        }
    }
}