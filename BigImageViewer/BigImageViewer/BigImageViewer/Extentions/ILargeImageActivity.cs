﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BigImageViewer.Extentions
{
    public interface ILargeImageActivity
    {
        void StartNativeIntentOrActivity();
    }
}
