﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigImageViewer
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SyncView : ContentPage
	{
        class ImageModel
        {
            public ImageSource Image1 { get; set; }

            public ImageModel()
            {
                Image1 = ImageSource.FromResource("BigImageViewer.img.MP600_300dpi.png");
            }
        }
        public SyncView ()
		{
			InitializeComponent ();
                BindingContext = new ImageModel();
            //imgViewer.Source= ImageSource.FromResource("BigImageViewer.testimage.jpg");
        }
	}
}