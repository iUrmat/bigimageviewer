﻿using BigImageViewer.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BigImageViewer
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            image1.Clicked += Image1_Clicked;
            image2.Clicked += Image21_Clicked;
            image3.Clicked += Image3_Clicked;
            PageFFImage.Clicked += PageFFImage_Clicked;
            StartNativeActivity.Clicked += StartNativeActivity_Clicked;
            SyncfusionView.Clicked += SyncfusionView_Clicked;
        }

        private async void SyncfusionView_Clicked(object sender, EventArgs e)
        {
            var syncview = new SyncView();
            await Navigation.PushAsync(syncview);
        }

        private void StartNativeActivity_Clicked(object sender, EventArgs e)
        {
            Xamarin.Forms.DependencyService.Register<ILargeImageActivity>();
            DependencyService.Get<ILargeImageActivity>().StartNativeIntentOrActivity();
        }

        private async void PageFFImage_Clicked(object sender, EventArgs e)
        {
            var imagepage1 = new PageFFImage();
            await Navigation.PushAsync(imagepage1);
        }

        private async void  Image21_Clicked(object sender, EventArgs e)
        {
            var imagepage1 = new Image1();
            await Navigation.PushAsync(imagepage1);
        }

        private async void Image1_Clicked(object sender, EventArgs e)
        {
            
            var imagepage1 = new Image2();
           await   Navigation.PushAsync(imagepage1);
        }
        private async void Image3_Clicked(object sender, EventArgs e)
        {

            var imagepage1 = new Image3();
            await Navigation.PushAsync(imagepage1);
        }
    }
}
