﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BigImageViewer.Droid;
using BigImageViewer.Extentions;
using Xamarin.Forms;
[assembly: Xamarin.Forms.Dependency(typeof(NativePages))]
namespace BigImageViewer.Droid
{
    public class NativePages : ILargeImageActivity
    {
        public NativePages()
        {
        }

        public void StartNativeIntentOrActivity()
        {
            var intent = new Intent(Forms.Context, typeof(LargeImageActivity));
            Forms.Context.StartActivity(intent);

        }
    }
}