﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using BigImageViewer.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static Android.Views.ScaleGestureDetector;

[assembly: ExportRenderer(typeof(Android.Widget.ScrollView), typeof(ZoomScrollViewRenderer))]
namespace BigImageViewer.Droid
{
    public class ZoomScrollViewRenderer : ScrollViewRenderer, IOnScaleGestureListener
    {
        //zoom için kullanacağımız değer
        private float mScale = 1f;
        private ScaleGestureDetector mScaleDetector;
        public ZoomScrollViewRenderer(Context context): base(context)
        {

        }
       
        public override bool DispatchTouchEvent(MotionEvent e)
        {
            base.DispatchTouchEvent(e);
            return mScaleDetector.OnTouchEvent(e);
        }
        public bool OnScale(ScaleGestureDetector detector)
        {
            //burada zoom yapınca limite daynama durumlarına karşı kontroller bulunmakta
            float scale = 1 - detector.ScaleFactor;
            float prevScale = mScale;
            mScale += scale;
            //yaklaşması veya max seviyede başa gelmesi 
            if (mScale < 0.5f)
            {
                mScale += scale;
            }
            if (mScale > 1)
            {
                mScale = 1f;
            }
            ScaleAnimation scalanim = new ScaleAnimation(1f / scale, 1f / mScale, 1f / prevScale, 1f / mScale, detector.FocusX, detector.FocusY);
            scalanim.Duration = 0;
            scalanim.FillAfter = true;
            StartAnimation(scalanim);
            return true;
        }
        public bool OnScaleBegin(ScaleGestureDetector detector)
        {
            return true;
        }
        public void OnScaleEnd(ScaleGestureDetector detector)
        {
        }
    }
}